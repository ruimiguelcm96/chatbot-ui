import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import { Button, TextField } from "@mui/material";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import React, { useState } from "react";



const KeyValueFormInput = (props) => {
  console.log("props", props)
  const { queryParameters, removeQueryParameter, addQueryParameter } = props;

  const [queryParameterValue, setQueryParameterValue] = useState("");
  const [queryParameterKey, setQueryParameterKey] = useState("");

  // const [dictionary, setDictionary] = useState([
  //   { key: 'city', value: 'Barcelona' },
  //   { key: 'name', value: 'José' }
  // ]);

  const removeParameterIndex = (entry) => {
    const newDictionary = dictionary.filter((d) => d.key !== entry.key && d.value !== entry.value);
    setDictionary(newDictionary)
  }

  const handleClickAddQueryParameter = () => {
    addQueryParameter(
      {
        key: queryParameterKey,
        value: queryParameterValue
      }
    )
    setQueryParameterKey("");
    setQueryParameterValue("");
  }

  return (
    <>
      <List>
        <ListItem>
          <TextField
            id="outlined-disabled"
            label="Key"
            onChange={(event) => setQueryParameterKey(event.target.value)}
            value={queryParameterKey}
          />
          <TextField
            id="outlined-disabled"
            label="Value"
            onChange={(event) => setQueryParameterValue(event.target.value)}
            value={queryParameterValue}
          />
          <Button
            disabled={!queryParameterKey || !queryParameterValue}
            onClick={handleClickAddQueryParameter}
          >Add</Button>
        </ListItem>
      </List>
      <List>
        {
          queryParameters.map((entry, index) => (
            <ListItem
              key={index}
              secondaryAction={
                <RemoveCircleOutlineIcon
                  onClick={() => removeParameterIndex(entry)}
                />
              }
            >
              <ListItemText primary={`${entry.key}: ${entry.value}`} />
            </ListItem>
          ))
        }
      </List>
    </>
  )
}

export default KeyValueFormInput;