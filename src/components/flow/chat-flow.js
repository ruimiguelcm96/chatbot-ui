import { Box, Button } from "@mui/material";
import React, { useCallback, useRef, useState, dispatch } from "react";
import ReactFlow, {
  Controls, removeElements
} from "react-flow-renderer";
import ModalApiRequest from "./modal-api-request";


const onLoad = (reactFlowInstance) => console.log("flow loaded:", reactFlowInstance);

const style = {
  background: "white",
  width: "100%",
  height: 300,
};

let elementsData = [];



export const ChatFlow = () => {
  const [els, setEls] = useState([]);
  //const onConnect = (params) => setEls((els) => addEdge(params, els));
  const onElementsRemove = (elementsToRemove) =>
    setEls((els) => removeElements(elementsToRemove, els));
  const yPos = useRef(0);


  // state for API req

  // state and funcs to manage api request modal
  const [openModalApiRequest, setOpenModalApiRequest] = useState(false);
  const [nodeSelected, setNodeSelected] = useState({id: "", data: {}});
  // const [params, setParams] = useState({});


  const handleCloseApiRequestModal = () => {
    setOpenModalApiRequest(false)
  }

  const onSubmitApiRequestForm = () => { }

  // const onConnect = (params) => {
  //   console.log("params", params)
  // }

  const onConnect = (params) => { 
    // Here, addEdge should be a Redux action, and needs to be dispatched
    addEdge(params, els);
  };

  const handleConfigureApiRequest = (nodeId) => {
    const node = elementsData.find(el => el.id == nodeId);
    setNodeSelected(node);
    setOpenModalApiRequest(true);
  }

  const addNode = () => {
    const nodeId = Math.floor(1000 + Math.random() * 9000);

    yPos.current += 50;
    setEls((els) => [...els, {
      id: `${nodeId}`,
      position: { x: 100, y: yPos.current },
      data: {
        label: (
          <>
            <div>Api Request #{nodeId}</div>
            <Button onClick={() => handleConfigureApiRequest(nodeId)}>Configure</Button>
          </>
        )
      },
      type: 'default'
    }]);
    elementsData = [
      ...elementsData,
      {
        id: nodeId,
        data: {
          httpVerb: "",
          endpoint: "",
          queryParameters: [],
          inputs: []
        }
      }
    ]
  };

  const addEdge = useCallback(({ source, target }) => {
    console.log("source", source, "target", target);
    setEls((els) => {
      console.log(source, target);
      return [
        ...els,
        {
          id: Math.random(),
          source,
          target
        }
      ];
    });
    const nodeIndex = elementsData.findIndex((node => node.id == target))
    elementsData[nodeIndex].data.inputs.push(source);
  }, []);

  return (
    <div>
      <ModalApiRequest
        open={openModalApiRequest}
        handleClose={handleCloseApiRequestModal}
        node={nodeSelected}
        // params={params}
        onSubmit={onSubmitApiRequestForm}
      />
      <div className="reactflow-wrapper">
        <ReactFlow
          style={style}
          elements={els}
          onConnect={onConnect}
          onElementsRemove={onElementsRemove}
          onLoad={onLoad}
        >
          <Controls />
        </ReactFlow>
      </div>
      <Box sx={{ mt: 3 }}>
        <Button color="primary" variant="contained" onClick={addNode}>
          Add API request
        </Button>
      </Box>
    </div>
  );
};