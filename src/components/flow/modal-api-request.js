import { Box, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, MenuItem, TextField } from "@mui/material";
import Divider from '@mui/material/Divider';
import React, { useState } from "react";
import KeyValueFormInput from './key-value-form-input';


const httpVerbs = [
  { label: 'POST', value: 'post' }
]

const ModalApiRequest = (props) => {
  const { open, handleClose, node } = props;

  const [httpVerb, setHttpVerb] = useState(node.data.httpVerb || "");
  const [endpoint, setEndpoint] = useState(node.data.endpoint || "");
  const [queryParameters, setQueryParameters] = useState(node.data.queryParameters || []);

  const inputs = node.data.inputs || [];


  const removeQueryParameter = (entry) => {
    const newDictionary = dictionary.filter((d) => d.key !== entry.key && d.value !== entry.value);
    setDictionary(newDictionary)
  }

  const addQueryParameter = () => {
    let newDictionary = [...dictionary];
    newDictionary.push({
      key: queryParameterKey, value: queryParameterValue
    })
    setDictionary(newDictionary);
    setQueryParameterKey("");
    setQueryParameterValue("");
  }

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>API Request Action</DialogTitle>
      <DialogContent>
        <DialogContentText>
          This form allow create a new API Request Action.
          You should set the http verb, http enpoint, path parameters and query parameters if any.
          {
            inputs.length > 0 && (
              <>
                It's possible to use the source action(s) on this action using the value(s): {inputs.map(i=>"$"+i).join(', ')}
              </>
            )
          }
        </DialogContentText>

        <Box mt={2} mb={2}>
          <TextField
            id="outlined-select-currency"
            select
            label="Select"
            value={httpVerb}
            onChange={(event) => setHttpVerb(event.target.value)}
            helperText="Please select the http verb"
          >
            {httpVerbs.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </Box>
        <Divider />
        <Box mt={2} mb={2}>
          <TextField
            label="Endpoint"
            value={endpoint}
            onChange={(event) => setEndpoint(event.target.value)}
            helperText="Please insert the enpoint"
          />
        </Box>
        <Divider />
        <Box mt={2}>
          <KeyValueFormInput 
            queryParameters={queryParameters}
            removeQueryParameter={removeQueryParameter}
            addQueryParameter={addQueryParameter}
          />
        </Box>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={handleClose} disabled={!httpVerb || !endpoint}>Subscribe</Button>
      </DialogActions>
    </Dialog>);
}

export default ModalApiRequest;